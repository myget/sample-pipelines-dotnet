# MyGet - NuGet/dotnet pipelines sample

This project contains an example of building, packaging and publishing a .NET Core library to [MyGet](http://www.myget.org) using Bitbucket Pipelines. Once published to MyGet, the package can be used in other projects using the [NuGet](http://ww.nuget.org) package manager.

Want to learn more? Here's a [full write-up on how a .NET Core build would work](https://blog.maartenballiauw.be/post/2016/08/17/building-nuget-netcore-using-atlassian-bitbucket-pipelines.html).

The pipelines configuration:

* Uses the [.NET Core Docker image](https://hub.docker.com/r/microsoft/dotnet/)
* Generates a buld number that is suffixed to the library package version
* Runs the following commands:
	* `dotnet restore`
	* `dotnet build`
	* `dotnet test` (optional, disabled by default)
	* `dotnet pack`
	* Publishes the package to a [MyGet](http://www.myget.org) feed

Before being able to invoke the Bitbucket pipeline, there are a couple of environment variables to be configured:

* `MYGET_NUGET_URL`: The full URL of the NuGet feed on MyGet (e.g. `https://www.myget.org/F/myfeed/api/v2`)
* `MYGET_NUGET_APIKEY`: MyGet API key
* `BUILD_CONFIGURATION`: Build configuration (Debug/Release)
